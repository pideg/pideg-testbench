"""
SPDX-License-Identifier: GPL-3.0-or-later
Copyright © 2021 Ashwin Pillay [fname][lname]@protonmail[dot]com
"""

import numpy as np
from matplotlib import pyplot as plt

a = -0.00782405
takeover_samples_list = np.array([int(np.round(250 * np.exp(a * t))) for t in range(1000)])
takeover_samples_list[-1] = 0
print(np.array2string(takeover_samples_list, separator=", "))
plt.plot(takeover_samples_list)
plt.show()
