"""
SPDX-License-Identifier: GPL-3.0-or-later
Copyright © 2021 Ashwin Pillay [fname][lname]@protonmail[dot]com
"""

from matplotlib import style
import math
from collections import deque

style.use('ggplot')

class PIDEnvelope:
    def __init__(self, input=None, Kp=0.001, Ki=0.0004, Kd=0.001, Kr=10, Kf=10, wet=1, limiterOn=True):

        self.sr = 441000
        self.limiterOn = limiterOn

        self.inputPIDO = input
        self.inputPIDOMaps = [False, False, False, False, False]

        self.parameters = [Kp, Ki, Kd, Kr, Kf, wet]

        self.dt = 0.1 * self.sr
        self.setPoint = 0
        self.measuredValue = 0
        self.output = 0
        self.integral = 0
        self.previousError = 0
        self.errors = deque(maxlen=3)
        for i in range(self.errors.maxlen):
            self.errors.append(0)

        self.timeState = 0
        self.target = 1
        self.changeOverValue = 0

    def value(self, return_setpoints=False):
        tempParameters = self.parameters
        if self.inputPIDO is not None:
            val = self.inputPIDO.value()
            for i in range(len(tempParameters)):
                if self.inputPIDOMaps[i]:
                    # print("TRUE FOR PARAMETER {}".format(i))
                    tempParameters[i] = val * self.inputPIDO.parameters[5] + \
                                        (1 - self.inputPIDO.parameters[5]) * self.parameters[i]

        if self.target == 1:
            self.setPoint = 1 - math.exp(-tempParameters[3] * self.timeState / self.sr)
            # self.setPoint = 1
        elif self.target == -1:
            self.changeOverValue = self.setPoint
            self.target = 0
        elif self.target == 0:
            self.setPoint = self.changeOverValue * math.exp(-tempParameters[4] * (self.timeState / self.sr))
            # self.setPoint = 0

        self.pid1(tempParameters=tempParameters)
        # self.pid2(tempParameters=tempParameters)

        self.measuredValue += self.output
        if self.limiterOn:
            self.measuredValue = max(0, min(1, self.measuredValue))

        self.timeState += 1

        if return_setpoints:
            return self.measuredValue, self.setPoint
        return self.measuredValue

    def keyOn(self):
        if self.inputPIDO is not None:
            self.inputPIDO.keyOn()
        self.target = 1
        self.timeState = 0

    def keyOff(self):
        if self.inputPIDO is not None:
            self.inputPIDO.keyOff()
        self.target = -1
        self.timeState = 0

    def mapInputOsc(self, Kp=False, Ki=False, Kd=False, Kr=False, Kf=False, oGain=False):
        self.inputPIDOMaps = [Kp, Ki, Kd, Kr, Kf, oGain]

    def pid2(self, tempParameters):
        T = 0.1
        K1 = tempParameters[0] + T * tempParameters[1] / 2 + tempParameters[2] / T
        K2 = -tempParameters[0] + T * tempParameters[1] / 2 - 2 * tempParameters[2] / T
        K3 = tempParameters[2] / T

        self.errors.append(self.setPoint - self.measuredValue)
        self.output = self.output + K1 * self.errors[2] + K2 * self.errors[1] + K3 * self.errors[0]

    def pid1(self, tempParameters):
        error = self.setPoint - self.measuredValue
        self.integral += error
        self.output = tempParameters[0] * error + tempParameters[1] * self.integral + \
                      tempParameters[2] * (error - self.previousError) / (self.dt / self.sr)
        # self.output = tempParameters[0] * error + tempParameters[1] * self.integral + tempParameters[2] * \
        #               (error - self.previousError)

        self.previousError = error

