"""
SPDX-License-Identifier: GPL-3.0-or-later
Copyright © 2021 Ashwin Pillay [fname][lname]@protonmail[dot]com
"""

from PySide2.QtGui import Qt
from PySide2.QtWidgets import QVBoxLayout, QHBoxLayout, QGroupBox, QPushButton
from commons.graphing.QGraphing import QGrapher
from commons.controls.QPIDControl import QPIDControl
from pideg.pidenvelope import PIDEnvelope


class QPIDEnvelope(QGroupBox):
    def __init__(self, envName='Untitled OSC', inputEnv=None,
                 parameterNames=['Kp', 'Ki', 'Kd', 'Kr', 'Kf', 'Dry/Wet'],
                 specs=[[-10, 4], [-10, 4], [-10, 4], [0, 12], [0, 12], [-10, 0]]):
        self.envName = envName.strip()
        super(QPIDEnvelope, self).__init__(self.envName)

        self.envRows = QHBoxLayout()
        self.graphing = QGrapher()
        self.controls = dict()

        self.parameterNames = parameterNames

        self.setpointCurveDisplayToggleButton = QPushButton("Show Setpoint Curve")
        self.setpointCurveDisplay = False
        self.setpointCurveDisplayToggleButton.pressed.connect(self.updateSetpointCurveDisplayState)

        self.childEnv = inputEnv
        self.parentEnv = None

        self.envOutputSamples = list()
        self.envSetpointSamples = list()

        for i in range(len(self.parameterNames)):
            self.controls[self.parameterNames[i]] = QPIDControl(self.parameterNames[i], spec=specs[i])
            self.envRows.addWidget(self.controls[self.parameterNames[i]])
            self.controls[self.parameterNames[i]].drawGraphSignal.interfaced.connect(self.updateEnvGraphing)

        self.completeEnvLayout = QVBoxLayout()
        self.completeEnvLayout.addLayout(self.envRows)
        self.completeEnvLayout.addWidget(self.graphing)
        self.completeEnvLayout.addWidget(self.setpointCurveDisplayToggleButton, Qt.AnchorRight)
        self.setLayout(self.completeEnvLayout)

    def linkChildToParent(self, parentEnv):
        self.parentEnv = parentEnv
        for i in range(len(self.parameterNames)):
            self.envRows.addWidget(self.controls[self.parameterNames[i]])
            self.controls[self.parameterNames[i]].knob.valueChanged.connect(self.parentEnv.updateEnvGraphing)
            self.controls[self.parameterNames[i]].display.textEdited.connect(self.parentEnv.updateEnvGraphing)
        print("[{}->LINK] => Link created from {} to {}".format(self.envName, self.envName, self.parentEnv.envName))

    def updateEnvGraphing(self):
        self.runPIDEnvelope()
        self.graphing.set_graph(src='OP', y=self.envOutputSamples)
        if self.setpointCurveDisplay:
            self.graphing.set_graph(src='SP', y=self.envSetpointSamples)
        print("[{}->GRAPH UPDATE]".format(self.envName))

    def updateSetpointCurveDisplayState(self):
        if self.setpointCurveDisplay:
            self.setpointCurveDisplay = False
            self.graphing.set_graph(src='SP', y=[0])
            self.setpointCurveDisplayToggleButton.setText("Show Leader Curve")
        else:
            self.setpointCurveDisplay = True
            self.graphing.set_graph(src='SP', y=self.envSetpointSamples)
            self.setpointCurveDisplayToggleButton.setText("Hide Leader Curve")
        print("[{}->CURVE VISIBILITY CHANGE] => Setpoint visibility set to {}".format(self.envName,
                                                                                      self.setpointCurveDisplay))

    def runPIDEnvelope(self):
        self.envOutputSamples.clear()
        self.envSetpointSamples.clear()
        if self.childEnv is not None:
            controlPIDO = PIDEnvelope(Kp=self.childEnv.controls['Kp'].controlValue,
                                      Ki=self.childEnv.controls['Ki'].controlValue,
                                      Kd=self.childEnv.controls['Kd'].controlValue,
                                      Kr=self.childEnv.controls['Kr'].controlValue,
                                      Kf=self.childEnv.controls['Kf'].controlValue,
                                      wet=self.childEnv.controls['Dry/Wet'].controlValue, limiterOn=False)
            mainPIDO = PIDEnvelope(input=controlPIDO, Kp=self.controls['Kp'].controlValue,
                                   Ki=self.controls['Ki'].controlValue,
                                   Kd=self.controls['Kd'].controlValue,
                                   Kr=self.controls['Kr'].controlValue,
                                   Kf=self.controls['Kf'].controlValue)
            mainPIDO.mapInputOsc(Ki=True, Kd=True)
        else:
            mainPIDO = PIDEnvelope(Kp=self.controls['Kp'].controlValue,
                                   Ki=self.controls['Ki'].controlValue,
                                   Kd=self.controls['Kd'].controlValue,
                                   Kr=self.controls['Kr'].controlValue,
                                   Kf=self.controls['Kf'].controlValue)
        mainPIDO.keyOn()
        for i in range(1000):
            mv, sv = mainPIDO.value(return_setpoints=True)
            self.envOutputSamples.append(mv)
            self.envSetpointSamples.append(sv)
        mainPIDO.keyOff()
        for i in range(1000):
            mv, sv = mainPIDO.value(return_setpoints=True)
            self.envOutputSamples.append(mv)
            self.envSetpointSamples.append(sv)
