"""
SPDX-License-Identifier: GPL-3.0-or-later
Copyright © 2021 Ashwin Pillay [fname][lname]@protonmail[dot]com
"""

import sys
from PySide2.QtWidgets import QApplication, QWidget, QHBoxLayout, QVBoxLayout
from pideg.QPIDEnvelope import QPIDEnvelope
from commons.player.QSoundPlayer import QSoundPlayer
from commons.theme.dark import setDarkPalette

app = QApplication()
app.setApplicationName("PIDEG")
setDarkPalette(app)

masterLayout = QVBoxLayout()
PIDOscLayout = QHBoxLayout()

# osc2 = QPIDEnvelope(envName='Control PIDEG')
# osc1 = QPIDEnvelope(envName='Main PIDEG', inputEnv=osc2)
# osc2.linkChildToParent(osc1)
osc1 = QPIDEnvelope(envName='Main PIDEG')
PIDOscLayout.addWidget(osc1)
# PIDOscLayout.addWidget(osc2)

player = QSoundPlayer(soundDrivers=osc1)

masterLayout.addLayout(PIDOscLayout)
masterLayout.addWidget(player)
mainWidget = QWidget()
mainWidget.setLayout(masterLayout)
mainWidget.show()

sys.exit(app.exec_())
